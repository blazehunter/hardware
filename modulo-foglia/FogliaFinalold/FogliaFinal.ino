#include <ServoTimer2.h>


#include <SimpleDHT.h>
#include <VirtualWire.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#define DHTTYPE DHT11
SimpleDHT11 dht11;
static const int RXPin = 3, TXPin = 4;
static const uint32_t GPSBaud = 9600;
TinyGPSPlus gps;
SoftwareSerial ss(RXPin, TXPin);
//String array to transmit


//Here to define the leaf id.
char* msg;
String latitude;
String longitude;
byte t = 0;
byte h = 0;
int ti = 0;
int hi = 0;
int posVari=0;
int buzzer=7;
int servoPin  = 9;
int servoGrad=0;
int servoUS=0;
int val0;
int val1;
int tolleranza = 40;
int co=0;
ServoTimer2 myservo;

void setup()
{
    Serial.begin(115200);    // Debugging only
    Serial.println("- BlazeHunter -");
    Serial.println("Modulo foglia\nAvvio in corso...");
    ss.begin(GPSBaud);
    // Initialise the IO and ISR
    vw_set_ptt_inverted(true); // Required for DR3100
    vw_set_tx_pin(5);
    vw_setup(2000);  // Bits per sec
    //init servo
    pinMode( A2, INPUT);
    pinMode( A3, INPUT);
    pinMode( 7, OUTPUT);
    myservo.attach( servoPin );
    myservo.write( 1150 ); 
  myservo.detach();
  delay(2000); 
}

void loop()
{
 //read dht and fill variables
  dht11.read(2, &t, &h, NULL);
    ti=(int)t-7;
    hi=(int)h;
    Serial.print("La temperatura è:");
    Serial.println(ti);
    Serial.print("L' umidità è:");
    Serial.println(hi);
    Serial.print("Il monossido di carbonio è:");
    Serial.println(co);
    //ring buzzer
    
    if(analogRead(A0)>125){
      digitalWrite(7, HIGH);
      co=analogRead(A0);  
    }
    else
      co=100;
    //read gps if available
    while (ss.available() > 0)
    if (gps.encode(ss.read()))
      displayInfo();
      

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    Serial.println(F("No GPS detected: check wiring."));
    while(true);
  }
    
    //concat string to send
    String string ;
    string.concat("1"); //set DEVICE ID
    string.concat(latitude);
    string.concat("-");
    string.concat(longitude);
    if(hi<10)
      string.concat("0");
    string.concat(hi);
    if(ti<10)
      string.concat("0");
    string.concat(ti);
    string.concat(co);
    msg=string.c_str();
    int k;
    
    //send string via radio
    Serial.print("Sto per trasmettere questa stringa via radio: ");
    Serial.println(string);
    digitalWrite(13, true); // Flash a light to show transmitting
    vw_send((uint8_t *)msg, 27);
    vw_wait_tx(); // Wait until the whole message is gone
    digitalWrite(13, false);
    delay(200); 
    digitalWrite(7,LOW);
    //read ldr and mv servo
  val0 = analogRead(A2);
  val1 = analogRead(A3);
  if(val0>val1&&servoGrad<170){
    servoGrad= servoGrad+5;
    
  }
 if(val1>(val0+27)&&servoGrad>20){
    servoGrad= servoGrad-5;
    
  }
  
  servoUS = map(servoGrad, 20, 170, MIN_PULSE_WIDTH, MAX_PULSE_WIDTH+200);
  Serial.print("La posizione in gradi della cella fv è: ");
  Serial.println(servoGrad);
  myservo.attach( servoPin );
  myservo.write( servoUS ); 
  delay(20);
  myservo.detach();
  delay(20);
    
}

void displayInfo(){
  
    if (gps.location.isValid())
          {
            latitude=String(gps.location.lat(), 6);
            longitude=String(gps.location.lng(), 6);
            Serial.println("La posizione è stata acquisita.");
            delay(1000);
            posVari++;
          }
          else{
        latitude =  {"41.234139"};
        longitude =  {"16.307882"};
        Serial.println("La posizione non può essere acquisita.");
      }
      if(posVari>=5){
        posVari=0;
        Serial.println("La posizione non è variata.");
      }
    
      }
