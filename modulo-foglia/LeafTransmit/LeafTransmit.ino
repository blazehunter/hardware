#include <SimpleDHT.h>
#include <VirtualWire.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#define DHTTYPE DHT11
SimpleDHT11 dht11;
static const int RXPin = 3, TXPin = 4;
static const uint32_t GPSBaud = 9600;
TinyGPSPlus gps;
SoftwareSerial ss(RXPin, TXPin);
//String array to transmit


//Here to define the leaf id.
char* msg;
String latitude;
String longitude;
byte t = 0;
byte h = 0;
int ti = 0;
int hi = 0;
int posVari=0;
void setup()
{
    Serial.begin(115200);    // Debugging only
    Serial.println("setup");
ss.begin(GPSBaud);
    // Initialise the IO and ISR
    vw_set_ptt_inverted(true); // Required for DR3100
    vw_set_tx_pin(5);
    vw_setup(2000);  // Bits per sec
}

void loop()
{
  dht11.read(2, &t, &h, NULL);
    ti=(int)t;
    hi=(int)h;
    Serial.println(ti);
    Serial.println(hi);
    
    
    while (ss.available() > 0)
    if (gps.encode(ss.read()))
      displayInfo();
      

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    Serial.println(F("No GPS detected: check wiring."));
    while(true);
  }
    
    
    String string ;
    string.concat("1-"); //set DEVICE ID
    string.concat(latitude);
    string.concat("-");
    string.concat(longitude);
    string.concat("-");
    if(hi<10)
      string.concat("0");
    string.concat(hi);
    string.concat("-");
    if(ti<10)
      string.concat("0");
    string.concat(ti);
    string.concat("-");
    string.concat(analogRead(A0));
      
    msg=string.c_str();
    int k;
    
    Serial.println(string);
    digitalWrite(13, true); // Flash a light to show transmitting
    vw_send((uint8_t *)msg, strlen(msg));
    vw_wait_tx(); // Wait until the whole message is gone
    digitalWrite(13, false);
    //delay(1000);
}
void displayInfo(){
  
    if (gps.location.isValid())
          {
            //Serial.print(gps.location.lat(), 6);
            //Serial.print(F(","));
            //Serial.print(gps.location.lng(), 6);
           
            latitude=String(gps.location.lat(), 6);
            longitude=String(gps.location.lng(), 6);
            Serial.println("pos okay");
            //Serial.println("okayy");
            delay(1000);
            posVari++;
          }
          else{
        latitude =  {"41.234139"};
        longitude =  {"16.307882"};
        Serial.println("pos non valida");
      }
      if(posVari>=5){
        posVari=0;
        //latitude =  {"41.234139"};
        //longitude =  {"16.307882"};
        Serial.println("pos non variata");
      }
    
      }
