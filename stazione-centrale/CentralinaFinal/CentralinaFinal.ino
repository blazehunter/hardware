/********************************
 * Libraries included
 *******************************/
#include <Ethernet.h>
#include <SPI.h>
#include <UbidotsEthernet.h>
#include <VirtualWire.h>
#include <ServoTimer2.h>
/********************************
 * Device Parameters
 *******************************/
/* Network parameters */
char const * TOKEN = "A1E-7abA0Y57TAZRFqbjh25RheRIA2oOsO"; // Assign your Ubidots TOKEN
char  * VARIABLE_LABEL = "position"; // Assign the unique variable label to send the data
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
/* Device ID Parameters*/

char const* deviceID="FE_ED_drone";

char received[28]="120.000000-50.0002006224444";//"DeviceID-LAT-LNG-HUM-TEMP-CO"

/* Ubidots Instance */
Ubidots client(TOKEN);
/* Stepper Parameters */
#define ENABLE 4
#define STEP 3
#define DIR 2
#define STEPS_N 9000
/*Servo declaration */
ServoTimer2 myservo;
int pos=0;
int i;
int servoUS;
float choice;
int counter=0;
/*Drone declaration */
boolean dronePresent=true;//Think about saving in EEPROM m8...
boolean hangarOpened=false;//Think about saving in EEPROM m8...
void setup() {
    //pinMode definitions
    pinMode(ENABLE,OUTPUT);
    pinMode(STEP,OUTPUT);
    pinMode(DIR,OUTPUT);
    digitalWrite(ENABLE,HIGH);
    Serial.begin(9600);
    //client.setDebug(true);// uncomment this line to visualize the debug message
    client.setDeviceLabel("BlazeHunter");
    /* start the Ethernet connection */
    Serial.println(F("- BlazeHunter -\nStazione centrale + modulo drone"));
    Serial.println("Test del modulo drone, ci vorrà qualche secondo...");
    openLid();
    //droneUp();
    delay(3000);
    //droneDown();
    closeLid();
    Serial.println("Test del modulo drone completato.");
    Serial.println("Avvio della shield ethernet...");
    if (!Ethernet.begin(mac)) {
      Serial.println(F("Problemi nell' attivare la shield ethernet."));
    } 
    else {
      Serial.println(Ethernet.localIP());
    }
    //    Initialise the RADIO RECEIVER
    Serial.print("Avvio del chip radio 433MHz...");
      vw_set_ptt_inverted(true); // Required for DR3100
      vw_set_rx_pin(5);
      vw_setup(2000);   // Bits per sec !!!HAVE GREAT POWER SUPPLY!!!
      
      vw_rx_start();       // Start the receiver PLL running
    /* Give the Ethernet shield a second to initialize */
    delay(2000);
    Serial.println(F("Pronto a ricevere"));
    //ENABLE DRONE ON CENTRALINA FE_ED
    //client.add(deviceID, 1);
    //client.sendAll();
     Serial.print("\nIn attesa di ricevere informazioni via radio...\n");
    
}

//Software functions
void openLid(){
  myservo.attach(9);
  servoUS = map(180, 0, 180, MIN_PULSE_WIDTH, MAX_PULSE_WIDTH);
  myservo.write(servoUS);
  delay(1000);
  myservo.detach();
}
void closeLid(){
  myservo.attach(9);
  servoUS = map(50, 0, 180, MIN_PULSE_WIDTH, MAX_PULSE_WIDTH);
  myservo.write(servoUS);
  delay(1000);
  myservo.detach();
}
void droneUp(){
  for( int j=0;j<15;j++){
  digitalWrite(ENABLE, LOW);
  digitalWrite(DIR,LOW);
  
  for(i=0;i<STEPS_N;++i) {
    digitalWrite(STEP,HIGH);
    delayMicroseconds(100);
    digitalWrite(STEP,LOW);
    delayMicroseconds(100);
    
  }
  digitalWrite(ENABLE,HIGH);
  }
}
void droneDown(){
  for( int j=0;j<15;j++){
  digitalWrite(ENABLE,LOW);
  digitalWrite(DIR,HIGH);
  
  for(i=0;i<STEPS_N;++i) {
    digitalWrite(STEP,HIGH);
    delayMicroseconds(100);
    digitalWrite(STEP,LOW);
    delayMicroseconds(100);
    
    
  }
  digitalWrite(ENABLE,HIGH);
  }
}

double charToDouble(char v[],int inizio, int fine){
String result="";
for(inizio;inizio<=fine;inizio++)
  result.concat(v[inizio]);
  return (result.toDouble());
}

char* charPointer(char v[],int inizio, int fine){
String result="";
for(inizio;inizio<=fine;inizio++){
  result.concat(v[inizio]);
}
char* ret;
  ret = new char [result.length()+1];
  strcpy (ret, result.c_str());
return ret;
}
char* charLatLng(char v[],int inizio, int fine){
String result="";
  for(inizio;inizio<=fine;inizio++)
    result.concat(v[inizio]);
  char* ret;
  ret = new char [result.length()+1];
  strcpy (ret, result.c_str());
  return ret;
}
 char* variableID(String type,char number){
  String result="";
  result.concat(number);
  result.concat("_");
  result.concat(type);
  char* ret;
  ret = new char [result.length()+1];
  strcpy (ret, result.c_str());
  return ret;
}

void loop() {
 
  Ethernet.maintain();
  //receive RADIO
  
    uint8_t buf[VW_MAX_MESSAGE_LEN];
    uint8_t buflen = VW_MAX_MESSAGE_LEN;
    if (vw_get_message(buf, &buflen)) // Non-blocking
    { 
           choice= client.getValue("BlazeHunter",deviceID);
            int i;
           
           // Message with a good checksum received, dump it.
            Serial.print("Ho ricevuto via radio: ");
            
            for (i = 0; i <= 27; i++)
                {
                  received[i]=(char)buf[i];
                  Serial.print(received[i]);
                }
                Serial.print("\nProvo a mandarlo in rete...\n");
                 // client.getValue("BlazeHunter",deviceID);
                char context[35];
            client.add( variableID("co",received[0]),charToDouble(received,24,26));
            client.add(variableID("temp",received[0]),charToDouble(received,22,23));
            client.add(variableID("hum",received[0]),charToDouble(received,20,21));
            sprintf(context, "\"lat\":%s,\"lng\":%s", charLatLng(received,1,9), charLatLng(received,11,19));
            client.add(variableID("pos",received[0]), 1, context);
            client.sendAll();
            
            Serial.print("E' necessario inviare il drone?");
            if(choice==1.0&&dronePresent){
                Serial.println("  SI!");
                Serial.println("Apro il coperchio dell' hangar");
                openLid();
                Serial.println("Alzo la piattaforma drone");
                droneUp();
                Serial.println("DECOLLO!");
                //droneTakeoff()
                //drone blades spin up
                dronePresent=false;//this maybe disconnects the power supply?idk
                hangarOpened=true;
                
            }
            else
                Serial.println("  NO!");
            delay(5000);
            
    }
 
 
    if(hangarOpened){
      Serial.println("Drone atterrato.");
      droneDown();
      closeLid();
      client.add(deviceID, 0);
      client.sendAll();
    }
}
