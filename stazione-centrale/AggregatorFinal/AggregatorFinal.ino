/********************************
 * Libraries included
 *******************************/
#include <Ethernet.h>
#include <SPI.h>
#include <UbidotsEthernet.h>
#include <VirtualWire.h>
/********************************
 * Constants and objects
 *******************************/
/* Assigns the Ubidots parameters */
char const * TOKEN = "A1E-7abA0Y57TAZRFqbjh25RheRIA2oOsO"; // Assign your Ubidots TOKEN
char  * VARIABLE_LABEL = "position"; // Assign the unique variable label to send the data

/* Enter a MAC address for your controller below */
/* Newer Ethernet shields have a MAC address printed on a sticker on the shield */
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

/* initialize the instance */
Ubidots client(TOKEN);

char received[32]="2-20.000000-50.000200-62-24-444";//"DeviceID-LAT-LNG-HUM-TEMP"

/********************************
 * Main Functions
 *******************************/
void setup() {
  Serial.begin(9600);
  client.setDebug(true);// uncomment this line to visualize the debug message
  client.setDeviceLabel("BlazeHunter");
  /* start the Ethernet connection */
  Serial.print(F("Starting ethernet..."));
  if (!Ethernet.begin(mac)) {
    Serial.println(F("failed"));
  } else {
    Serial.println(Ethernet.localIP());
  }
  //    Initialise the RADIO RECEIVER
    vw_set_ptt_inverted(true); // Required for DR3100
    vw_set_rx_pin(5);
    vw_setup(2000);   // Bits per sec !!!HAVE GREAT POWER SUPPLY!!!
    
    vw_rx_start();       // Start the receiver PLL running
  /* Give the Ethernet shield a second to initialize */
  delay(2000);
  Serial.println(F("Ready"));
}

double charToDouble(char v[],int inizio, int fine){
String result="";
for(inizio;inizio<=fine;inizio++)
  result.concat(v[inizio]);
  return (result.toDouble());
}

char* charPointer(char v[],int inizio, int fine){
String result="";
for(inizio;inizio<=fine;inizio++){
  result.concat(v[inizio]);
}
char* ret;
  Serial.println(result);
  ret = new char [result.length()+1];
  strcpy (ret, result.c_str());
return ret;
}
char* charLatLng(char v[],int inizio, int fine){
String result="";
  for(inizio;inizio<=fine;inizio++)
    result.concat(v[inizio]);
  Serial.println(result);
  char* ret;
  ret = new char [result.length()+1];
  strcpy (ret, result.c_str());
  return ret;
}
 char* variableID(String type,char number){
  String result="";
  result.concat(number);
  result.concat("_");
  result.concat(type);
  char* ret;
  Serial.println(result);
  ret = new char [result.length()+1];
  strcpy (ret, result.c_str());
  return ret;
}

void loop() {
  Ethernet.maintain();
  //receive RADIO
    uint8_t buf[VW_MAX_MESSAGE_LEN];
    uint8_t buflen = VW_MAX_MESSAGE_LEN;

    if (vw_get_message(buf, &buflen)) // Non-blocking
    { 
            
            int i;
           
           // Message with a good checksum received, dump it.
            Serial.print("Got: ");
            
            for (i = 0; i < buflen; i++)
                {
                  received[i]=(char)buf[i];
                  Serial.print(received[i]);
                }
                char context[35];
            client.add( variableID("co",received[0]),charToDouble(received,28,30));//here is the error, if you type a value within quotes it works, otherwise not.co
            client.add(variableID("temp",received[0]),charToDouble(received,25,26));
            client.add(variableID("hum",received[0]),charToDouble(received,22,24));
            sprintf(context, "\"lat\":%s,\"lng\":%s", charLatLng(received,2,10), charLatLng(received,12,20));
            client.add(variableID("pos",received[0]), 1, context);
            client.sendAll();
            
    }
    
  delay(5000);
}
