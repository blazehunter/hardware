/********************************
 * Libraries included
 *******************************/
#include <Ethernet.h>
#include <SPI.h>
#include <UbidotsEthernet.h>
#include <VirtualWire.h>
/********************************
 * Constants and objects
 *******************************/
/* Assigns the Ubidots parameters */
char const * TOKEN = "A1E-7abA0Y57TAZRFqbjh25RheRIA2oOsO"; // Assign your Ubidots TOKEN
char const * one_hum = "Leaf1_hum"; // Assign the unique variable label to send the data
char const * one_temp = "leaf1_temp"; // Assign the unique variable label to send the data
char const * one_lat = "leaf1_lat"; // Assign the unique variable label to send the data
char const * one_lng = "leaf1_lng"; // Assign the unique variable label to send the data
char const * one_co = "leaf1_co"; // Assign the unique variable label to send the data

char const * two_hum = "Leaf2_hum"; // Assign the unique variable label to send the data
char const * two_temp = "leaf2_temp"; // Assign the unique variable label to send the data
char const * two_lat = "leaf2_lat"; // Assign the unique variable label to send the data
char const * two_lng = "leaf2_lng"; // Assign the unique variable label to send the data
char const * two_co = "leaf2_co"; // Assign the unique variable label to send the data

char const * three_hum = "Leaf3_hum"; // Assign the unique variable label to send the data
char const * three_temp = "leaf3_temp"; // Assign the unique variable label to send the data
char const * three_lat = "leaf3_lat"; // Assign the unique variable label to send the data
char const * three_lng = "leaf3_lng"; // Assign the unique variable label to send the data
char const * three_co = "leaf3_co"; // Assign the unique variable label to send the data

char   received[31]="2-20.000000-50.000200-62-24-444";//"DeviceID-LAT-LNG-HUM-TEMP"
char devicetemp;
char devicehum;
char devicelat;
char devicelng;
char deviceco;
/* Enter a MAC address for your controller below */
/* Newer Ethernet shields have a MAC address printed on a sticker on the shield */
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

/* initialize the instance */
Ubidots client(TOKEN);

/********************************
 * Main Functions
 *******************************/
void setup() {
  Serial.begin(9600);
  client.setDebug(true);// uncomment this line to visualize the debug message
  client.setDeviceLabel("BlazeHunter");
  /* start the Ethernet connection */
  Serial.print(F("Starting ethernet..."));
  if (!Ethernet.begin(mac)) {
    Serial.println(F("failed"));
  } else {
    Serial.println(Ethernet.localIP());
  }
  /* Give the Ethernet shield a second to initialize */
  delay(2000);
//    Initialise the RADIO RECEIVER
    vw_set_ptt_inverted(true); // Required for DR3100
    vw_set_rx_pin(5);
    vw_setup(2000);   // Bits per sec !!!HAVE GREAT POWER SUPPLY!!!
    
    vw_rx_start();       // Start the receiver PLL running
  delay(2000);
  Serial.println(F("Ready"));
}
double charToDouble(char v[],int inizio, int fine){
String result="";
for(inizio;inizio<=fine;inizio++){
  result.concat(v[inizio]);
}

return (result.toDouble());
}


void loop() {

  Ethernet.maintain();
  //receive RADIO
    uint8_t buf[VW_MAX_MESSAGE_LEN];
    uint8_t buflen = VW_MAX_MESSAGE_LEN;

    if (vw_get_message(buf, &buflen)) // Non-blocking
    {
            int i;
            char context[30];
            
           // Message with a good checksum received, dump it.
            Serial.print("Got: ");
            
            for (i = 0; i < buflen; i++)
                {
                  received[i]=(char)buf[i];
                  Serial.print(received[i]);
                }
            //checking the deviceID
            Serial.print("The Device ID is: ");
            Serial.println(received[0]);
            client.add(received[0]+"_co",charToDouble(received,28,30));
            client.add(received[0]+"_temp",charToDouble(received,25,26));
            client.add(received[0]+"_hum",charToDouble(received,22,24));
            sprintf(context, "lat=%s$lng=%s", char(charToDouble(received,2,10)), char(charToDouble(received,12,20)));
            client.add(received[0]+"_pos", 0.0, context);
            //client.add(received[0]+"_lat",charToDouble(received,2,10));
            //client.add(received[0]+"_lng",charToDouble(received,12,20));
            client.sendAll();
    }
    
            
           if(490==received[0]){ //49= 1 in ASCII
              client.add(one_lat, 61054655);//obv we will send the lat achieved from radio
              client.add(one_lng, 24054655);//obv we will send the lng achieved from radio
              client.add(one_temp, 25.00);//obv we will send the temp achieved from radio
              client.add(one_hum, 62.3);//obv we will send the hum achieved from radio
              client.add(one_co, 185);//obv we will send the hum achieved from radio
              
              Serial.print("The Device set the data to send ");
               }
                else if(500==received[0]){ //50=2 in ASCII
                  client.add(two_lat, 61054655);//obv we will send the lat achieved from radio
                  client.add(two_lng, 24054655);//obv we will send the lng achieved from radio
                  client.add(two_temp, 25.00);//obv we will send the temp achieved from radio
                  client.add(two_hum, 62.3);//obv we will send the hum achieved from radio
                  client.add(two_co, 185);//obv we will send the hum achieved from radio
               }
                  else if(510==received[0]){ //51=3 in ASCII
                    client.add(three_lat, 61054655);//obv we will send the lat achieved from radio
                    client.add(three_lng, 24054655);//obv we will send the lng achieved from radio
                    client.add(three_temp, 25.00);//obv we will send the temp achieved from radio
                    client.add(three_hum, 62.3);//obv we will send the hum achieved from radio
                    client.add(three_co, 185);//obv we will send the hum achieved from radio
                 }
            
           
            
            delay(3000);
          }
        



